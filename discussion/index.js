
//arithmetic operators

let x = 123412;
let y = 745612;

let sum = x + y;
console.log(`This is the sum: ${sum}`);

let difference = y - x;
console.log("Subtractor operator " + difference);

let product = x * y;
console.log("Multiplication operator " + product);

let quotient = y / x;
console.log("Division " + quotient);

let remainder = y % x;
console.log("Mudulo operator " + remainder);


let assignmentNumber = 8;

assignmentNumber += 2;
console.log("Addition assignment " + assignmentNumber);

assignmentNumber += 2;
console.log("Addition assignment " + assignmentNumber);

assignmentNumber -= 2;
console.log("Subtraction assignment " + assignmentNumber);

assignmentNumber *= 2;
console.log("Multiplication assignment " + assignmentNumber);

assignmentNumber /= 2;
console.log("Division assignment " + assignmentNumber);

let string1 = "Food";
let string2 = "truck";
string1 += string2;
console.log(string1);

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("MDAS: " + mdas);


let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("PEMDAS: " + pemdas);

console.log(Math.ceil(pemdas));


let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1;
console.log(numE)

let numF = false + 1;
console.log(numF);

//Equality Operator (==)

console.log(1 == 1);
console.log(1 == 2);
console.log(Math.PI == 3.14);
console.log(1 == "1");
console.log(false == 0);
console.log("John" == "John");
console.log("john" == "John");


//inequality Operator (!=)

console.log(1 != 1);
console.log(1 != 2);
console.log(Math.PI != 3.14);
console.log(1 != "1");
console.log(false != 0);
console.log("John" != "John");
console.log("john" != "John");

//Strict Equality Operator (===)
let John = "John"
console.log(1 === 1); // result: true
console.log(1 === "1"); // result: false
console.log("John" === "John");
console.log("John" === John);
console.log(false === 0);

//strict inequality operator (!==)

console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1') //true
console.log("John" !== John); //false

// Relational Operator
let a = 50;
let b = 65;

//GT or Greater Than (>)
let isGreaterThan = a > b;
console.log(isGreaterThan); //false

//LT or Less Than (<)
let isLessThan = a < b;
console.log(isLessThan); //true

//GTE or Greater Than or Equal (>=)
let isGTE = a>=b;
console.log(isGTE); //false

//LTE or  Less than or Equal (<=)
let isLTE = a <= b;
console.log(isLTE); //true

let numStr = "30"
console.log(a > numStr);